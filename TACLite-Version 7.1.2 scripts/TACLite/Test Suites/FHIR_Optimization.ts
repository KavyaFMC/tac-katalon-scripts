<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>FHIR_Optimization</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>29f98d5c-9298-4252-83a3-07c99b9b31e2</testSuiteGuid>
   <testCaseLink>
      <guid>3ad88f9e-0938-401e-b796-01ac25ea47b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0c2de63-eb2b-478f-ba00-768880e1f002</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/Final AVF(Active),AVF(TemporarilyUnUsable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b36215b-6928-482c-ab7f-c911e9b0836c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abd42903-7177-4e95-bb72-67fb489f6664</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30b3fceb-9e00-4931-99c5-77a9a1c5c3d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b69601fb-87ee-44ed-8c9f-78760cea60b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bbb3afa-a194-44b9-a0f0-739fa1bc9441</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>699ff77b-574b-4d55-a03f-ff802fa3e071</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3e4b4bc-3861-4629-934e-4948585bcf68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fae3cd67-6e5f-4659-b20f-897c2d12d9cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e59091cf-879b-462c-b471-c71e8420c50a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),CVC(WaitingForRemoval)/Final AVF(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dfaa0d9-9b3b-4e7a-b5a2-464db4adb131</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>672eaafe-2d81-4ea2-bea7-92ec92a6b8fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/Final-AVF(Active),AVF(TemporaryUnusable)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
