<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TAC E2E</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>64512b03-3007-4a83-b350-f3d373b75257</testSuiteGuid>
   <testCaseLink>
      <guid>d6961cb1-b135-415d-b93c-d100220661ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),CVC(WaitingForRemoval)/Final AVF(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62a1f210-f627-4750-9d8e-552ade85081c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1be8e439-d877-45a8-80ae-a98bdd69a683</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>313f2fd4-dd63-4381-8e57-3c9501b2882d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/Final-AVF(Active),AVF(TemporaryUnusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2902e22a-ae5b-40bb-98bd-1cc5022668db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36cf942d-dcb6-498c-81ca-72faf0bf8118</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>563ca954-6947-46d9-9670-36086d2a75cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
