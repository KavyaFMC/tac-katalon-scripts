<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Both needle - No UsualFindings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6b03a1ac-0b45-47a0-a5f2-65f0160e8e6c</testSuiteGuid>
   <testCaseLink>
      <guid>72eb9cdb-932a-4971-80ee-559b3638e590</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/Final AVF(Active),AVF(TemporarilyUnUsable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebff40c0-9bfc-4cdb-9bc1-a89bd97db666</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab1dfad6-2b91-431b-86b7-489577e82407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
