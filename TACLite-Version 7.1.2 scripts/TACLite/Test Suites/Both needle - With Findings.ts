<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Both needle - With Findings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>53e541ae-5072-46b7-a398-02a5e3203225</testSuiteGuid>
   <testCaseLink>
      <guid>a20b982f-fe5e-477c-908d-76297452c23c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a84327d1-1975-4c72-b7d1-62475607cf86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccb8bc39-4a83-4fe5-9e56-0d4ee685d9aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>380095bd-50c9-4688-b9a7-e89ec0e5bb90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
