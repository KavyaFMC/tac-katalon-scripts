package reusableComponents

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence


public class Signature {
	@Keyword
	public void TACLiteSignature(String xpath,String Username,String Password,String Type){
		TestObject testObj=new TestObject()
		testObj.addProperty("xpath", ConditionType.EQUALS, (Type))
		WebUI.click(testObj);

		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> options = driver.findElements(By.xpath((xpath)))
		println((xpath))
		println(options.size())




		for(WebElement x :options){

			println('@'+x.getText())


			if(x.getText().equals((value))){
				x.click()
				break;
			}
		}
	}

	@Keyword
	public  static void clickButton(){
		//take button list , exa: 3 buttons
		TestObject testObj=new TestObject()
		WebDriver driver=DriverFactory.getWebDriver()

		List<WebElement> btnList1 = driver.findElements(By.xpath('//*[@id="interventionsScroller"]//div[@class="cs-table"]//button'));
		int btnCount=btnList1.size();
		if (btnCount>0) {
			while (btnCount>0){
				driver.navigate().refresh();
				btnList1 = driver.findElements(By.xpath('//*[@id="interventionsScroller"]//div[@class="cs-table"]//button'));
				int count=btnList1.size();
				if(count==0)break;
				//click on first button
				btnList1.get(0).click();
				WebUI.scrollToElement(null, 1)
				testObj.addProperty("xpath", ConditionType.EQUALS, '//*[@id="cancelBtn"]')
				WebUI.click(testObj);
				WebUI.delay(3)
				//other statements
				btnCount--;
			}
		}
		println("Came out from Do while Loop");
	}

	@Keyword
	def static checkCondition(){
		for(int i=0;i<3;i++){

			if(i==1){
				break
				print "inside loop"} else{

				continue
			}

			print "Out loop"
		}
	}

}

