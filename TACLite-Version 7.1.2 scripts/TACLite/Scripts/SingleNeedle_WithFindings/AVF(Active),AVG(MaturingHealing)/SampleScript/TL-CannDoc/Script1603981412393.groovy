import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import utilityKeywords.helperUtilities as helperUtilities

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\Cannulation.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CannDoc'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CannDoc'), '', 'click')
WebUI.delay(3)


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Select1'), '', 'click')
WebUI.delay(3)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_Findings', true)
def CannCleaned1=excelData.getValue('CannCleaned1', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_AccessCleaned')+CannCleaned1+prop.getProperty('Cann'), '', 'click')

def ArterialAttempts=excelData.getValue('ArterialAttempts', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialAttempts1')+ArterialAttempts+prop.getProperty('Cann'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FinalAttempt1'), '', 'click')

WebUI.delay(5)
def ArterialNeedleSize=excelData.getValue('ArterialNeedleSize', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialNeedleSize')+ArterialNeedleSize+prop.getProperty('Cann'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialNeedleSize')+ArterialNeedleSize+prop.getProperty('Cann'), '', 'click')
def Cann_Outcome1=excelData.getValue('Cann_Outcome1', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Successful')+Cann_Outcome1+prop.getProperty('Cann'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Select2'), '', 'click')
WebUI.delay(3)
def CannCleaned2=excelData.getValue('CannCleaned2', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_AccessCleaned')+CannCleaned2+prop.getProperty('Cann'), '', 'click')

def VenousAttempts=excelData.getValue('VenousAttempts', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('VenousAttempts1')+VenousAttempts+prop.getProperty('Cann'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FinalAttempt1'), '', 'click')
def VenousNeedleSize=excelData.getValue('VenousNeedleSize', 2)
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('VenousNeedleSize')+VenousNeedleSize+prop.getProperty('Cann'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('VenousNeedleSize')+VenousNeedleSize+prop.getProperty('Cann'), '', 'click')
def Cann_Outcome2=excelData.getValue('Cann_Outcome2', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Successful')+Cann_Outcome2+prop.getProperty('Cann'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)













