import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//TacLite_Properties//MaturationCannulation.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

Properties prop2 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//DynamicProperties.properties')

TestObject testObj=new TestObject()

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Maturation'), '', 'scrolltoelement')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Maturation'), '', 'click')

WebUI.delay(3)

WebUI.takeScreenshot()

def AVFistulaPresent=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('AVFistulaPresent'))

if (WebUI.waitForElementPresent(AVFistulaPresent, 3, FailureHandling.OPTIONAL)){

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('select1'), '', 'click')

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)

def Mat_Look1=excelData.getValue('Mat_Look1', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Look1+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Look2=excelData.getValue('Mat_Look2', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Look2+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Look3=excelData.getValue('Mat_Look3', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Look3+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Look4=excelData.getValue('Mat_Look4', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Look4+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Listen=excelData.getValue('Mat_Listen', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Listen+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Feel1=excelData.getValue('Mat_Feel1', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Feel1+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Feel2=excelData.getValue('Mat_Feel2', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Feel2+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Feel3=excelData.getValue('Mat_Feel3', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Feel3+prop.getProperty('AVG_Cann'), '', 'click')

def Mat_Feel4=excelData.getValue('Mat_Feel4', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Mat_Feel4+prop.getProperty('AVG_Cann'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)

WebUI.takeScreenshot()
}
