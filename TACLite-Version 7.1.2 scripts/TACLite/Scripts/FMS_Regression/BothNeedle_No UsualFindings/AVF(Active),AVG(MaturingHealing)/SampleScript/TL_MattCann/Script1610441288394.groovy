import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities

import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//ChairSide.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

Properties prop2 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//DynamicProperties.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Maturation'), '', 'scrolltoelement')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Maturation'), '', 'click')
//CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')
WebUI.delay(3)

WebUI.takeScreenshot()
//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('cannulation'), '', 'click')

WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('select1'), '', 'click')


Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def Cann_Look=excelData.getValue('Cann_Look', 4)
println(Cann_Look)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_look')+Cann_Look+prop.getProperty('AVG_Cann'), '', 'click')

//Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def Cann_Listen=excelData.getValue('Cann_Listen', 4)
println(Cann_Listen)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_listen')+Cann_Listen+prop.getProperty('AVG_Cann'), '', 'click')

//Object excelData2 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def Cann_Feel1=excelData.getValue('Cann_Feel1', 4)
println(Cann_Feel1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel1+prop.getProperty('AVG_Cann'), '', 'click')

//Object excelData3 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def Cann_Feel2=excelData.getValue('Cann_Feel2', 4)
println(Cann_Feel2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel2+prop.getProperty('AVG_Cann'), '', 'click')


///Object excelData4 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def Cann_Feel3=excelData.getValue('Cann_Feel3', 4)
println(Cann_Feel3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel3+prop.getProperty('AVG_Cann'), '', 'click')

//Object excelData5 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def Cann_Feel4=excelData.getValue('Cann_Feel4', 4)
println(Cann_Feel4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel4+prop.getProperty('AVG_Cann'), '', 'click')


CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)

WebUI.takeScreenshot()









