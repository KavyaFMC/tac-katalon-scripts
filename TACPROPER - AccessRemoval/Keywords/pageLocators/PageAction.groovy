package pageLocators

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling

import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import helperUtilities.HelperFunctions as HelperFunctions
import internal.GlobalVariable
import org.openqa.selenium.WebElement
import com.kms.katalon.core.testdata.reader.ExcelFactory

public class PageAction{
	Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\CommonFunctions\\EditAccess.properties')
	@Keyword
	public void pageOperation(String xpath,String value,String type){
		TestObject testObj=new TestObject()
		switch (type){

			case "input":
				testObj.addProperty("xpath", ConditionType.EQUALS, xpath)
				WebUI.setText(testObj,value);
				break;

			case "click":

				testObj.addProperty("xpath", ConditionType.EQUALS, xpath)
				WebUI.click(testObj);
				break;

			case "dropdown":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.selectOptionByLabel(testObj,value, false)
				break;

			case "switchframe":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.switchToFrame(testObj, 3)
				break;

			case "defaultcontent":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.switchToDefaultContent()
				break;

			case "cleardata":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.clearText(testObj)
				break;

			case "verifytext":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.verifyElementText(testObj, value)
				break;

			case "scrolltoelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.scrollToElement(testObj, 10)
				break;

			case "verifyelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)


				WebUI.verifyElementPresent(testObj, 7, FailureHandling.OPTIONAL)

				break;

			case "mouseOver":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
			//WebUI.takeScreenshot()

				WebUI.mouseOver(testObj)
				break;

			case "choosemenu":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.selectOptionByValue(testObj, value, false)
				break;

			case "valueselection":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.getText(testObj)
				break;

			case "conditioncheck":

				def a=testObj.addProperty('xpath', ConditionType.EQUALS, xpath)
				if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){
					println("Cartridge")
					WebUI.callTestCase(findTestCase('NxStageOrders/NonCARwithPureflow'), [:], FailureHandling.STOP_ON_FAILURE)
				}
				else{
					println("Dialyzer")
					WebUI.callTestCase(findTestCase('NxStageOrders/CARwithPureflow'), [:], FailureHandling.STOP_ON_FAILURE)
				}

			case "ifcheck":

				def b=testObj.addProperty('xpath', ConditionType.EQUALS, xpath)
				println(b)
				if (WebUI.waitForElementPresent(b, 3, FailureHandling.OPTIONAL)){

					println("if part")

				}
				else{
					println("else part")

				}

			case "check":

				def a=testObj.addProperty('xpath', ConditionType.EQUALS, xpath)
				if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){


				}
				else{
					println("Not AVG or AVF access")
				}





		}
	}

	@Keyword
	public String pageReturnAction(String xpath,String value,String type){
		TestObject testObj=new TestObject()
		switch (type){
			case "verifyelement":
				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				try{
					if(WebUI.waitForElementPresent(testObj, 5) == true){
						return true
					}
					else
					{
						return false
					}}
				catch(NoSuchElementException e)
				{
				}
				break;
		}

	}

	@Keyword

	public void accessRemoval(List <WebElement> activeElements,int activeCount) throws WebElementNotFoundException{

		TestObject editAccessBtn= new TestObject()
		editAccessBtn.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('editaccessdetails'))
		TestObject addNewStatusBtn= new TestObject()
		addNewStatusBtn.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('addnewstatus'))

		TestObject avfCheck= new TestObject()
		def avf=avfCheck.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('AVFcheck'))
		TestObject avgCheck= new TestObject()
		def avg=avgCheck.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('AVGcheck'))
		TestObject cvcCheck= new TestObject()
		def cvc=cvcCheck.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('CVCcheck'))
		TestObject pdCheck= new TestObject()
		def pd=pdCheck.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('PDcheck'))


		TestObject pustatusCheck= new TestObject()
		pustatusCheck.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('PUStatusCheck'))
		TestObject removedstatusCheck= new TestObject()
		removedstatusCheck.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('RemovedCheck'))

		//println WebUI.verifyElementPresent(avgCheck, 5)

		//println WebUI.verifyElementText(cvcCheck, 'AVG')

		TestObject backBtn=new TestObject()
		backBtn.addProperty("xpath", ConditionType.EQUALS,prop.getProperty('back'))

		TestObject closeBtn=new TestObject()
		closeBtn.addProperty("xpath", ConditionType.EQUALS,prop.getProperty('close'))

		TestObject popupBtn= new TestObject()
		popupBtn.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('savepopupyes'))

		TestObject puStatus=new TestObject()
		puStatus.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('pustatus'))

		TestObject removedStatus=new TestObject()
		removedStatus.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('removedstatus'))

		TestObject statusReason=new TestObject()
		statusReason.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('other'))

		TestObject statusStartDate=new TestObject()
		statusStartDate.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('statusdateastoday'))

		TestObject saveBtn=new TestObject()
		saveBtn.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('editaccesssave'))


		TestObject statusBtn = new TestObject()
		statusBtn.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('accessstatus'))
		TestObject statusreasonBtn = new TestObject()
		statusreasonBtn.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('statusreason'))
		TestObject yesBtn = new TestObject()
		yesBtn.addProperty("xpath",ConditionType.EQUALS,prop.getProperty('savepopupyes'))



		//println WebUI.verifyElementPresent(avg, 5,FailureHandling.CONTINUE_ON_FAILURE)
		int minAccess=activeCount


		int i=activeElements.size();

		for (int j = 0; j < i; j++){


			println (minAccess)
			if(minAccess>2){
				activeElements.get(j).click()


				println(i)

				WebUI.click(editAccessBtn)
				WebUI.click(addNewStatusBtn)
				WebUI.delay(5)
				try{
					if((WebUI.verifyElementPresent(avg, 5,FailureHandling.OPTIONAL)==true) || (WebUI.verifyElementPresent(avf, 5, FailureHandling.OPTIONAL)==true)){
						println "AVG or AVF"
						if((WebUI.verifyElementPresent(pustatusCheck, 5,FailureHandling.OPTIONAL)==true) || (WebUI.verifyElementPresent(removedstatusCheck, 5,FailureHandling.OPTIONAL)==true))
						{
							println "Else part"
							WebUI.click(backBtn)
							WebUI.click(closeBtn)

						}
						else
						{

							WebUI.click(statusBtn)
							WebUI.click(puStatus)
							WebUI.click(statusreasonBtn)
							WebUI.click(statusReason)
							WebUI.click(statusStartDate)
							WebUI.click(saveBtn)
							minAccess=minAccess-1
							if(WebUI.verifyElementPresent(yesBtn, 3, FailureHandling.OPTIONAL)==true){
								WebUI.click(yesBtn)
								WebUI.click(closeBtn)

							}
							else{
								WebUI.click(closeBtn)
							}
						}
					}

					else {
						println "CVC or PD"
						if(WebUI.verifyElementPresent(removedstatusCheck, 5,FailureHandling.OPTIONAL)==true)
						{
							println "Else part"
							WebUI.click(backBtn)
							WebUI.click(closeBtn)

						}
						else
						{
							WebUI.click(statusBtn)
							WebUI.click(removedStatus)
							WebUI.click(statusreasonBtn)
							WebUI.click(statusReason)
							WebUI.click(statusStartDate)
							WebUI.click(saveBtn)
							minAccess=minAccess-1
							if(WebUI.verifyElementPresent(yesBtn, 3, FailureHandling.OPTIONAL)==true){
								WebUI.click(yesBtn)
								WebUI.click(closeBtn)
							}
							else{
								WebUI.click(closeBtn)
							}
						}

					}
					WebUI.delay(5)

				}
				catch(WebElementNotFoundException e ){
					this.println (e)


				}
			}
			else
			{
				println "Excess Access removed"
				break
			}

		}}}


