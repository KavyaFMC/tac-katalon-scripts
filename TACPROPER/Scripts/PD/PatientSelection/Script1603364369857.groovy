import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions
import internal.GlobalVariable

Properties env  = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

//WebUI.openBrowser('http://dama2.qa-intranet.fmcna.com/tac-app')

WebUI.openBrowser(GlobalVariable.URL)
WebUI.waitForPageLoad(5)

WebUI.maximizeWindow()

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), 'tnurse', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('password'), 'Today123', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('signinbutton'), '', 'click')

WebUI.waitForPageLoad(5)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PD', true)
def Clinic=excelData.getValue("Clinic", 1)
println(Clinic)
def MRN=excelData.getValue("MRN", 1)
println(MRN)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectclinic'),Clinic, 'input')
WebUI.delay(2)
Robot rb = new Robot()

rb.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient'),MRN, 'input')
WebUI.delay(3)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient1'), '', 'click')


WebUI.delay(3)