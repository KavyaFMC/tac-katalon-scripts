package reusablekeyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import com.kms.katalon.core.testobject.ConditionType

public class DatePicker {
	
	@Keyword
	public void Dateselection(String xpath){

		YearMonth thisMonth = YearMonth.now()

		YearMonth lastMonth = thisMonth.minusMonths(1)

		//println lastMonth


		DateTimeFormatter abcx=DateTimeFormatter.ofPattern("MM")

		def finalmonth=lastMonth.format(abcx)

		println finalmonth


		DateFormat dateformat1=new SimpleDateFormat("dd")

		Date date= new Date()

		String todaydate=dateformat1.format(date)

		println todaydate
		DateFormat dateformat2=new SimpleDateFormat("yyyy")

		Date year= new Date()

		String Currentyear=dateformat2.format(year)

		println Currentyear


		println finalmonth+'/'+todaydate+'/'+Currentyear


		def DatePicker=finalmonth+'/'+todaydate+'/'+Currentyear

		println DatePicker


		TestObject testObj=new TestObject()

		testObj.addProperty("xpath", ConditionType.EQUALS, (xpath))

		WebUI.setText(testObj,DatePicker);
	}
}
