import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException as IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass as CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword as Keyword
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.testdata.InternalData as InternalData
import java.io.FileInputStream as FileInputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.FileOutputStream as FileOutputStream
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import java.sql.Driver as Driver
import java.util.concurrent.TimeUnit as TimeUnit
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.StaleElementReferenceException as StaleElementReferenceException
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory

Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//ChairSide_Properties//PatientSelection.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

TestObject testObj = new TestObject()

TestObject testObj1 = new TestObject()

WebUI.comment('Launch url and login to the application')

WebUI.openBrowser('')

WebUI.navigateToUrl(prop1.getProperty('QAURL'))

WebUI.maximizeWindow()

WebUI.waitForPageLoad(5)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Username'), prop1.getProperty('Username'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Password'), prop1.getProperty('Password'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Submit'), '', 'click')

WebUI.comment('Check if login is successful')

def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Clinic_landing'))

if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)) {
    WebUI.comment('Login to ChairSide application is successful')
} else {
    WebUI.closeBrowser()
}

WebUI.comment('Fetch the required values from data sheet')

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_Findings', 
    true)

//def Clinic = excelData.getValue('Clinic', 1)
def Clinic = 100030
//def Shift=excelData.getValue("Shift", 1)
def Shift = 3

def PatientName = excelData.getValue('PatientName', 1)

WebUI.comment('Clinic,shift and patient selection')

CustomKeywords.'pageLocators.pageOperation.pageAction'((((prop.getProperty('Clinic_selection') + Clinic) + ' - ') + prop.getProperty(
        'Single_quote')) + prop.getProperty('Single_braces'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(((((prop.getProperty('Shift') + prop.getProperty('Single_quote')) + 
    Shift) + ' - ') + prop.getProperty('Single_quote')) + prop.getProperty('shift1'), '', 'click')

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(((((prop.getProperty('Shift') + prop.getProperty('Single_quote')) + 
    Shift) + ' - ') + prop.getProperty('Single_quote')) + prop.getProperty('shift1'), '', 'click')

WebUI.waitForPageLoad(5)

WebUI.takeScreenshot()

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Absent_Button'))
//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PtSelect')+prop.getProperty('Single_quote')+PatientName+prop.getProperty('Single_quote')+prop.getProperty('PtSelect1'), '', 'click')
WebUI.delay(2)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS)

driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS)

//driver.navigate().refresh()
List<WebElement> listBtn = driver.findElements(By.xpath(prop.getProperty('Absent_Button')))

//println(listBtn.size())
/*if(listBtn.size()==0){
			println "Buttons count is: "+listBtn.size()
			println "LoopOut"
			break
		}
		*/
//if(listBtn.get(btnNumber-1).enabled)
println(listBtn.size())

int btnList = listBtn.size()

while (btnList > 0) {
    try {
        List<WebElement> listBtn1 = driver.findElements(By.xpath(prop.getProperty('Absent_Button')))

        for (int j = listBtn1.size(); j > 0; j--) {
            println('printing within loop ' + listBtn1.size())

            WebUI.delay(10)

            //JavascriptExecutor js = (JavascriptExecutor)driver;
            //js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",listBtn1.get(j-1))
            if (listBtn1.get(j - 1).isEnabled() == true) {
                listBtn1.get(j - 1).click()

                CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AbsentYesBtn'), '', 'click')

                WebUI.delay(5)

                //println(listBtn.get(j))
                CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SignatureUserName'), prop1.getProperty(
                        'Username'), 'input')

                CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SignaturePassword'), prop1.getProperty(
                        'Password'), 'input')

                CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SignOffBtn'), '', 'click')
            } else {
                println('This is not clickable')
            }
        }
    }
    catch (StaleElementReferenceException e) {
        WebUI.refresh()
    } 
    
    btnList--
}
WebElement shiftCloseBtn= driver.findElement(By.xpath(prop.getProperty('closeshiftbutton')))

if(shiftCloseBtn.isEnabled()==true){
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('closeshiftbutton'), '', 'click')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('closeshiftyes'), '', 'click')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SignatureUserName'), prop1.getProperty(
		'Username'), 'input')

	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SignaturePassword'), prop1.getProperty(
		'Password'), 'input')

	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SignOffBtn'), '', 'click')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('closeshiftalert'), '', 'click')
	
	
}
else
{
	println "Please complete the pending treatment to close the shift"
}