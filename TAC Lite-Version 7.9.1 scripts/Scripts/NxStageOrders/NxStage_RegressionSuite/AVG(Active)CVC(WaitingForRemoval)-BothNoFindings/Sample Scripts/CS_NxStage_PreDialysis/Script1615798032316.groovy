import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException as IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.annotation.Keyword as Keyword
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.testdata.InternalData as InternalData
import java.io.FileInputStream as FileInputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.FileOutputStream as FileOutputStream
import java.lang.CharSequence as CharSequence
import java.sql.Driver as Driver
import java.util.concurrent.TimeUnit as TimeUnit
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import utilityKeywords.helperUtilities as helperUtilities

import java.time.LocalDateTime as LocalDateTime
import java.time.format.DateTimeFormatter as DateTimeFormatter

import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory

WebUI.comment('Load the required property files')
Properties prop4 = helperUtilities.getObj2('C://katalon//TACLite//Data Resources//ChairSide_Properties//CS_StaticValues.properties')
Properties prop5 = helperUtilities.getObj3('C://katalon//TACLite//Data Resources//ChairSide_Properties//PreDialysis.properties')
Properties propNx2 = helperUtilities.getNxProp3('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_MachineSetup.properties')
Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//PatientSelection.properties')
Properties propNx = helperUtilities.getNxProp1('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_PropertyValues.properties')
Properties propNx1 = helperUtilities.getNxProp2('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_EnvVariables.properties')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('PreDialysis'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Update_button1'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

WebUI.comment('Write the Preweight value into excel')

WebUI.callTestCase(findTestCase('CommonFunctions/writeValuesNxStage'), [:], FailureHandling.STOP_ON_FAILURE)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet(propNx1.getProperty('Excelreadpath'), "Data", true)

WebUI.comment('Fetch the Preweight value from excel')
def PreWeight = excelData.getValue('PreWeight', 1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('PreWeight'), ''+PreWeight, 'input')

WebUI.comment('Enter the Pre Dialysis tab field values')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Resp'), prop4.getProperty('RespNo'), 'input')

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('O2'), prop4.getProperty('02No'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('SitBP'), prop4.getProperty('SitBPNo'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('SitDiastolic'), prop4.getProperty('SitDiastolicNo'), 
    'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Temp'), prop4.getProperty('Tempvalue'), 'input')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Confirmation1'), '', 'click')

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Confirmation2'),'','click')
WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('StandingBP'), prop4.getProperty('StandingBPNo'), 
    'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('StandingDiastolic'), prop4.getProperty('StandingDiastolicNo'), 
    'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Pulse'), prop4.getProperty('PulseNo'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('IR'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('NxStage_TWR'), prop4.getProperty('NxStage_TWR'), 
    'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('TimeObserved'), '', 'click')


WebUI.comment('Write the Time value into excel')
WebUI.callTestCase(findTestCase('CommonFunctions/writeTimeValuesNxStage'), [:], FailureHandling.STOP_ON_FAILURE)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('TimeObserved'), '', 'click')
Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet(propNx1.getProperty('Excelreadpath'), "Data1", true)

WebUI.comment('Write the Time value into excel')

def Time = excelData1.getValue('Time', 1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('TimeObserved'), '' + Time, 'input')

WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Signature'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.CS'(propNx1.getProperty('Username'), propNx1.getProperty('Password'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Pre_Continue'), '', 'click')

WebUI.delay(2)

//----------------------------------Update button 2-----------------------------------------------

WebUI.comment('Enter the values in the Pre Dialysis Update')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Update_button2'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Findings'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('TimeObserved2'), '' + Time, 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Signature'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.CS'(propNx1.getProperty('Username'), propNx1.getProperty('Password'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop5.getProperty('Pre_Continue'), '', 'click')

WebUI.delay(2)

WebUI.takeScreenshot()








