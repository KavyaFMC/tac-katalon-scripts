import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import excelHandle.excelGetValues as excelGetValues

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\MachineSetup.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

Properties  staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\CS_EnvVariables.properties')
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('MachineSetup_Update'),'', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Button'), '', 'defaultcontent')
WebUI.delay(2)


println(prop.getProperty('Machine_number'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_number'),staticvalues.getProperty("MachineNo"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Treatment_section'),staticvalues.getProperty("Treatment_section"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_Type'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_Conductivity'),staticvalues.getProperty("Machine_Conductivity"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Meter_Conductivity'),staticvalues.getProperty("Meter_Conductivity"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_Temp'),staticvalues.getProperty("Machine_Temp"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ph'),staticvalues.getProperty("ph_No"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('NVL'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Alarms'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PHT'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AirDetectorArmed'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('HighFlux'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Bleach_NA'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PerformedBy'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Performed_Continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Update'), '', 'switchframe')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_Ok'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('MachineSetup_Update'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Button'), '', 'defaultcontent')
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ReviewedBy'), '', 'click')
WebUI.waitForPageLoad(5)
WebUI.delay(2)
CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("ReviewedUser"),prop1.getProperty("Password"))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Performed_Continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Update'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
WebUI.delay(2)

WebUI.takeScreenshot()




