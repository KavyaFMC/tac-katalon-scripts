import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import reusablekeyword.Reusable as Reusable

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

Properties prop2 = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\FluidManagement.properties')

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop2.getProperty('TLFrame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreTxButton'), '', 'click')