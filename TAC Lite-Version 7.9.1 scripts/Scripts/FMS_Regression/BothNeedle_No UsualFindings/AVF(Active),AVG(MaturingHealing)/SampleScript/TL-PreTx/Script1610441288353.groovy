import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')
WebUI.delay(3)

WebUI.takeScreenshot()



WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')


Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Look1=excelData.getValue('PreTx_Look1', 4)
println(PreTx_Look1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look1+prop.getProperty('Pre'), '', 'click')

//Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Listen1=excelData.getValue('PreTx_Listen1', 4)
println(PreTx_Listen1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen1+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)

//Object excelData2 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Feel1=excelData.getValue('PreTx_Feel1', 4)
println(PreTx_Feel1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel1+prop.getProperty('Pre'), '', 'click')

//Object excelData3 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Other1=excelData.getValue('PreTx_Other1', 4)
println(PreTx_Other1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select2'), '', 'click')


//Object excelData4 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Look2=excelData.getValue('PreTx_Look2', 4)
println(PreTx_Look2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look2+prop.getProperty('Pre'), '', 'click')
//Object excelData5 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Listen2=excelData.getValue('PreTx_Listen2', 4)
println(PreTx_Listen2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen2+prop.getProperty('Pre'), '', 'click')
//Object excelData6 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Feel2=excelData.getValue('PreTx_Feel2', 4)
println(PreTx_Feel2)
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel2+prop.getProperty('Pre'), '', 'click')

//Object excelData7 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Other2=excelData.getValue('PreTx_Other2', 4)
println(PreTx_Other2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other2+prop.getProperty('Pre'), '', 'click')
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(2)
CustomKeywords.'reusablekeyword.Reusable.STARTTX'()
/*
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
LocalDateTime datetime = LocalDateTime.now();
//System.out.println("Before subtraction of hours from date: "+datetime.format(formatter));
datetime=datetime.minusHours(1);
String aftersubtraction=datetime.format(formatter);
//System.out.println("After 1 hour subtraction from date: "+aftersubtraction);
//println (''+PreTime);


/*String[] preTime = aftersubtraction.split("\\s+");
String split_Time=preTime[1];
Time=''+split_Time

String[] preTime = aftersubtraction.split("\\ ");
println(''+preTime[1])
//String split_Time=preTime[intschedarr[0]];
Time=''+preTime[1]
*/

Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet("C:\\katalon\\TACLite\\InputData\\ReadWrite.xlsx", "Data1", true)

def Time = excelData1.getValue('Time', 1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Starttx_time'),''+Time,'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Start_continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Access_Eval'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')




















